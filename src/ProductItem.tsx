import { View, Image, StyleSheet } from "react-native";
import { Surface, Text, IconButton } from "react-native-paper";
import { Inventory } from "./store/inventory";
import { useMemo, useState } from "react";

type ProductItemProps = {
  record: Inventory;
};

const NEW_PRODUCT_THRESHOLD_DAYS = 7;

/**
 * Determines if a product is considered "new" based on the posted date and a threshold in days.
 *
 * @param {string} date - A string representing the posted date in ISO format.
 * @param {number} thresholdDays - The number of days after which a product is no longer considered new.
 * @returns {boolean} `true` if the product is considered new; otherwise, `false`.
 */
function isNewProduct(date: string, thresholdDays: number) {
  const msInDay = 86400000;
  const timeDiffMs = new Date().getTime() - new Date(date).getTime();
  return timeDiffMs < thresholdDays * msInDay;
}

/**
 * Splits a string of categories into an array of tags using various delimiters.
 *
 * @param {string} categories - A string containing categories separated by commas, double spaces, tabs, or capital letters.
 * @returns {string[]} An array of tags extracted from the input string.
 */
function getCategoriesAsTags(categories: string): string[] {
  if (categories) {
    const commaTabSpaceSplit = categories.split(/,|\s{2,}|\t/);
    // Fallback for when neither comma, tag or tab splits the categories -> split using capital letters:
    const tags = (
      commaTabSpaceSplit.length > 0
        ? commaTabSpaceSplit
        : categories.split(/(?=[A-Z])/)
    ).map((item) => item.trim());
    return tags;
  }
  return [];
}

function ProductItem(props: ProductItemProps) {
  const [toggled, setToggled] = useState(false);
  const fallbackImage = require("../assets/fallback-image.png");
  const categories = useMemo(
    () => getCategoriesAsTags(props.record.fields["Product Categories"]),
    [props.record.fields["Product Categories"]]
  );
  const isNew = useMemo(
    () => isNewProduct(props.record.fields.Posted, NEW_PRODUCT_THRESHOLD_DAYS),
    [props.record.fields.Posted]
  );

  return (
    <Surface style={styles.itemContainer}>
      <View>
        <Image
          source={
            props.record.fields["Product Image"]
              ? { uri: props.record.fields["Product Image"] }
              : fallbackImage
          }
          defaultSource={fallbackImage}
          style={styles.productImage}
        />
      </View>

      <View style={styles.container}>
        {/* HEADER ELEMENT START */}
        <View style={styles.headerContainer}>
          <Text numberOfLines={toggled ? 0 : 1} style={styles.title}>
            {props.record.fields["Product Name"]}
          </Text>
          <View style={styles.headerRight}>
            {isNew && <Text style={styles.newTag}>New</Text>}
            <IconButton
              icon={toggled ? "chevron-up" : "chevron-down"}
              size={24}
              style={styles.toggleBtn}
              onPress={() => setToggled((prevToggle) => !prevToggle)}
            />
          </View>
        </View>
        {/* HEADER ELEMENT END */}

        <Text style={styles.date}>
          {new Date(props.record.fields.Posted).toLocaleDateString()}
        </Text>

        {/* TOGGLE BODY START */}
        {toggled && categories.length > 0 && (
          <View style={styles.tags}>
            {categories.map((tag, index) => (
              <View key={index} style={styles.tag}>
                <Text numberOfLines={1} style={styles.tagText}>
                  {tag}
                </Text>
              </View>
            ))}
          </View>
        )}
        {toggled && categories.length == 0 && (
          <Text style={styles.categoryInfo}>Keine Kategorien hinterlegt</Text>
        )}
        {/* TOGGLE BODY END */}
      </View>
    </Surface>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    padding: 8,
    flexDirection: "row",
    alignContent: "stretch",
    borderRadius: 4,
    gap: 12,
    overflow: "hidden",
    backgroundColor: "#f8f9fc"
  },
  productImage: {
    width: 85,
    minHeight: 85,
    height: "auto",
    objectFit: "contain"
  },
  container: {
    flexGrow: 1,
    width: 100
  },
  headerContainer: {
    flexDirection: "row",
    gap: 12,
    justifyContent: "space-between",
    alignItems: "flex-start",
    marginBottom: 4
  },
  title: {
    fontSize: 20,
    fontWeight: "900",
    flex: 1,
    flexWrap: "wrap",
    marginTop: 5
  },
  headerRight: {
    flexDirection: "row",
    alignItems: "center"
  },
  newTag: {
    paddingVertical: 8,
    paddingHorizontal: 12,
    color: "white",
    textTransform: "uppercase",
    borderRadius: 9,
    backgroundColor: "#333",
    borderTopRightRadius: 0,
    fontWeight: "700"
  },
  toggleBtn: {
    marginVertical: 0
  },
  date: {
    fontSize: 12
  },
  tags: {
    paddingTop: 12,
    flexDirection: "row",
    flexWrap: "wrap",
    gap: 6
  },
  tag: {
    height: 24,
    maxWidth: 200,
    borderRadius: 24 / 2,
    backgroundColor: "#d4e5ff"
  },
  tagText: {
    fontSize: 12,
    paddingHorizontal: 12,
    lineHeight: 24,
    textAlign: "center",
    flexWrap: "wrap"
  },
  categoryInfo: {
    paddingTop: 12,
    fontSize: 12
  }
});

export default ProductItem;
