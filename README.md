# Hybrid Heroes - Challenge

## Product list redesign

According to the Task [INV-1](https://gitlab.com/hybridheroes/coding-challenge#inv-1) the display of product items should be updated to represent a [new design](https://www.figma.com/file/K8J4g5y1QnYZonwgFisvXK/Coding-Challenge?node-id=0%3A1).

* Implementation: react-native
* Dev-Environment: Android Emulator API 30 (Pixel 3a)


A **video** of the working component can be found under: [doc/hybridheroes-challenge.mp4](https://gitlab.com/portfolio3171834/hybrid-heroes-challenge/-/blob/master/doc/hybridheroes-challenge.mp4)
